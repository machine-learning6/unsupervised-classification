import sklearn.datasets as ds
import copy
import random

# clustering from scratch

# step 1 : initialize centroids (= future classes)

# step 2 : determine the class of each point of the data_set

# step 3 : take the mean of each class and change the value of centroids

# step 4 : redo steps 2 and 3 until no more changes

# step 5 : display the results + compare the results with the true

def init_centroids(data_set, nb_classes): # function call step 1
    nb_axes = len(data_set[0])
    centroid_list = []
    for i in range(nb_classes):
        centroid_list.append(random.choice(data_set))

    return centroid_list


def find_class(data_set, centroid_list, nb_classes):  # function call step 2
    list_class = []
    for i in range(len(data_set)):
        memory_distance = 1000.0
        for j in range(nb_classes):
            distance = distance_2_points(data_set[i], centroid_list[j])
            if distance < memory_distance:
                memory_distance = distance
                memory_class = j
        list_class.append(memory_class)
    return list_class


def distance_2_points(pointA, pointB):
    distance = 0
    for i in range(len(pointA)):
        distance = distance + (pointA[i] - pointB[i])**2
    return distance**(1/2)


def clustering_algo_IRIS(data_set, nb_classes, centroid_list) :
    nb_axes = len(data_set[0])

    # step 2:
    list_class = find_class(data_set, centroid_list, nb_classes)

    # step 3:

    # division of the data_set in sub_list ordered (according to the class found in step 2)
    sub_list = []
    for index_class_tested in range(nb_classes):
        sub_list.append([])
        for index_point_tested in range(len(data_set)):
            if list_class[index_point_tested] == index_class_tested:
                sub_list[index_class_tested].append(data_set[index_point_tested])

    # copy the centroid_list to keep in mind and be able to compare at the end
    old_centroid_list = copy.deepcopy(centroid_list)

    # display the data
    # display_data(data_set, centroid_list)
    #display_data_iris(data_set, centroid_list)

    # calculation of the new centroids
    for i in range(nb_classes):
        for k in range(nb_axes):
            if (len(sub_list[i]) == 0): break  # if the class is empty, break the loop
            sum = 0
            for j in range(len(sub_list[i])):
                sum = sum + sub_list[i][j][k]
            centroid_list[i][k] = sum / len(sub_list[i])

    # step 4 : check the evolution and STOP or REDO step 2 and 3
    if centroid_list == old_centroid_list:
        return sub_list  # if no more evolution, return the data_set sorted
    else:
        return clustering_algo_IRIS(data_set, nb_classes, centroid_list)


def test_quality(sorted_data_set, nb_classes):
    for i in range(nb_classes):
        print("Number of elements in class ", i, " : ", len(sorted_data_set[i]))

def main(data_set, nb_classes):
    # step 1 : initialization of the centroids :
    centroid_list = init_centroids(data_set, nb_classes)

    # steps 2, 3, 4 :
    sorted_data_set = clustering_algo_IRIS(data_set, nb_classes, centroid_list)

    # check the quality :
    test_quality(sorted_data_set, nb_classes)

if __name__ == "__main__":

    # import IRIS
    iris = ds.load_iris()

    iris_data = iris['data'].tolist()
    iris_classes = iris['target']
    iris_classes_names = iris['target_names']
    iris_descr = iris['DESCR']


    nb_classes = 3
    main(iris_data, nb_classes)

